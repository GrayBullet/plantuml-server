FROM tomcat

RUN apt-get update && apt-get install -qy graphviz fonts-droid fonts-roboto fonts-wqy-microhei && rm -rf /var/lib/apt/list && apt-get clean
RUN rm -rf /usr/local/tomcat/webapps && mkdir /usr/local/tomcat/webapps
ADD server.xml /usr/local/tomcat/conf
ADD plantuml-server/target/plantuml.war /usr/local/tomcat/webapps
